FROM alpine:3.5

ENV DOCKER_VERSION 1.13.1
ENV DOCKER_COMPOSE_VERSION 1.11.1
ENV DOCKER_BUCKET experimental.docker.com

RUN echo "@edgecommunity http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && apk add --update --no-cache \
        curl \
        dcron \
        docker@edgecommunity \
        python \
        py-pip \
        bash \
        tzdata \
    && pip install docker-compose==${DOCKER_COMPOSE_VERSION} \
    && cp /usr/share/zoneinfo/America/New_York /etc/localtime \
    && echo "America/New_York" > /etc/timezone \
    && apk del tzdata

COPY crontab /root/crontab
COPY docker-entrypoint.sh /root/docker-entrypoint.sh
COPY run_compose_service /usr/local/bin/run_compose_service

ENV DOCKER_HOST ''

ENTRYPOINT ["/root/docker-entrypoint.sh"]

CMD ["tail", "-F", "/var/log/cron"]