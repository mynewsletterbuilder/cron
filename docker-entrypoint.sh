#!/bin/bash

export DOCKER_HOST=$DOCKER_HOST

# we make a bash script with env variables in it
# so cron can load them.
printenv | sed 's/^\(.*\)\=\(.*\)$/export \1\="\2"/g' > /root/project_env
chmod +x /root/project_env

crontab -f /root/crontab

touch /var/log/cron

crond -f >> /var/log/cron &

exec "$@"