# Basic Usage

This cron container is set up to run docker compose services as cron jobs.  You need to provide a docker compose file and a crontab to define your jobs.  You also need to give the container a docker engine/swarm master to talk to.

### Docker Compose File

The work done by your cron jobs should be defined as sanely named services in `/root/docker-compose.yml` like so:
```
version: '2'
services:
  cron-example:
    image: alpine:3.4
    command: echo "hello world"
```

### Crontab

You need a crontab that defines the timing of your services.  A script named `run_compose_service` is provided to run your services in the way this container expects.  You should use this script to run your services defined in `/root/crontab` like so:
```
# cron-example
* * * * * run_compose_service cron-example
```

### Docker/Swarm Access

There are two ways to give the cron server access to docker.  You can either mount your `docker.sock` to `/var/run/docker.sock` like so:

```
version: '2'
services:
  cron:
    image: jbanetwork/cron
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./crontab:/usr/root/crontab
      - ./example.docker-compose.yml:/root/docker-compose.yml
```


Or you can set the `DOCKER_HOST` env variable at a swarm host like so:

```
version: '2'
services:
  cron:
    image: jbanetwork/cron
    environment:
      - DOCKER_HOST=tcp://swarm:4000
    volumes:
      - ./crontab:/usr/root/crontab
      - ./example.docker-compose.yml:/root/docker-compose.yml
```

As of yet TLS is not supported.

# Advanced Usage

This repo contains a slightly more complex working example.

### mounting a local directory as a volume for use by the container started by cron

Because you are never ACTUALLY running docker in the cron container, the context of your source volume is relative to the docker host, not the container.  Wow that is a confusing sentence.  Basically using a REAL HOST volume to hold the code that is being run by a cron job requires 2 things

1. You have to run against a local docker engine using the "mount your docker.sock" method.  There is no way to accomplish this in swarm or on a remote host.

2. The compose file (or docker command) that is part of your cron job must mount the volume using the path on the HOST machine not the cron container. Luckily we have a working example of this for you. Notice how we pass `$PWD` (the current working dir) into the container as the env variable `CRON_BASE_DIR` in [docker-compose.yml](docker-compose.yml). Then in [example.docker-compose.yml](example.docker-compose.yml) we use THAT var as the target of our volume for the actual cron job service.

